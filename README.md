# ODDESTOODDS Test
**Candidate : Vatshal Basdeo**

## Source Code Version Control
In this project i have used a Branch by Functionality Version Control Pattern to isolate the functionalities being developed to a single branch which will thus afterward be merged back to a single repository (developbranch) before commiting onto Master(Trunk).

## Backend Architecture / Repository Architecture
The Backend architecture follows the onion architecture for this architecture is quite loosely coupled and provide a fast and versatile code base that can be quickly adapted to changes in requirements or specifiaction.
The Administrator **CRUD** functionalities uses Entity Framework 6.

## Dependency Injection
I have used Unity for the dependency injection 

## SignalR 
I have used SignalR for the front realtime display for the quotes 

# How to Run the application
- Restore the given .bak file on your SQL Server.
- - the bak file will already contain user Admin username = (admin@admin.com) and  pwd=(Qwerty01*)
- Change the connectionStrings in the WebConfig for WebUI.
- Then Run the Application
