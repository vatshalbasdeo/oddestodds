﻿-- =============================================
-- Author:		Basdeo Vatshal
-- Create date:	04/05/2019
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PUBLISHCHANGES]
	-- Add the parameters for the stored procedure here
@UserId uniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Create 
		SELECT * FROM QUOTESTOPUBLISH where userid = @UserId and ACTION = 1
		
		INSERT INTO QUOTES
			SELECT Id,Home,Away,HomeWinQuote,DrawQuote,AwayWinQuote FROM QUOTESTOPUBLISH
			WHERE userid = @UserId and ACTION = 1;
		
		DELETE FROM QUOTESTOPUBLISH where userid = @UserId and ACTION = 1

	--Edit 
		DELETE FROM QUOTES WHERE Id IN (SELECT ID FROM QUOTESTOPUBLISH where userid = @UserId and ACTION = 2);
		
		INSERT INTO QUOTES
			SELECT Id,Home,Away,HomeWinQuote,DrawQuote,AwayWinQuote FROM QUOTESTOPUBLISH
			WHERE userid = @UserId and ACTION = 2;
		
		DELETE FROM QUOTESTOPUBLISH where userid = @UserId and ACTION = 2

	--DELETE
		DELETE FROM QUOTES WHERE Id IN (SELECT ID FROM QUOTESTOPUBLISH where userid = @UserId and ACTION = 3);
		
		DELETE FROM QUOTESTOPUBLISH where userid = @UserId and ACTION = 3

    return 1 ;
END