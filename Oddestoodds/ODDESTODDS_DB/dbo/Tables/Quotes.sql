﻿CREATE TABLE [dbo].[Quotes] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Home]         VARCHAR (100)    NOT NULL,
    [Away]         VARCHAR (100)    NOT NULL,
    [HomeWinQuote] DECIMAL (18, 4)  NOT NULL,
    [DrawQuote]    DECIMAL (18, 4)  NOT NULL,
    [AwayWinQuote] DECIMAL (18, 4)  NOT NULL,
    CONSTRAINT [PK_QuotesTable] PRIMARY KEY CLUSTERED ([Id] ASC)
);

