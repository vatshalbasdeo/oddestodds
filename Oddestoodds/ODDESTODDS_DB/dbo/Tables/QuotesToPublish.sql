﻿CREATE TABLE [dbo].[QuotesToPublish]
(
	[Id]           UNIQUEIDENTIFIER NOT NULL,
    [Home]         VARCHAR (100)    NOT NULL,
    [Away]         VARCHAR (100)    NOT NULL,
    [HomeWinQuote] DECIMAL (18, 4)  NOT NULL,
    [DrawQuote]    DECIMAL (18, 4)  NOT NULL,
    [AwayWinQuote] DECIMAL (18, 4)  NOT NULL,
    [UserId]       NVARCHAR(128) NOT NULL,
    [Action]       INT              NOT NULL,
    CONSTRAINT [PK_QuotesNotPublished] PRIMARY KEY CLUSTERED ([Id] ASC)
)
