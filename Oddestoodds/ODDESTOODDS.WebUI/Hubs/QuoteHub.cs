﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ODDESTOODDS.WebUI.Hubs
{
    public class QuoteHub : Hub
    {
        public static void Show()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<QuoteHub>();
            context.Clients.All.displayQuote();
        }
    }
}