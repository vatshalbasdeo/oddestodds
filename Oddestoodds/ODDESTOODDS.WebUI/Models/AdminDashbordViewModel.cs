﻿using Oddestoodds.ServicesInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ODDESTOODDS.WebUI.Models
{
    public class AdminDashbordViewModel
    {
        public List<QuotesItem> Current { get; set; }
        public List<QuotesItem> NotPublished { get; set; }
    }
}