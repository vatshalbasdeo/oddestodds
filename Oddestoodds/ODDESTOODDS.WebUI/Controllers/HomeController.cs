﻿using ODDESTOODDS.WebUI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ODDESTOODDS.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get()
        {
            var result = new QuotesData().Get();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}