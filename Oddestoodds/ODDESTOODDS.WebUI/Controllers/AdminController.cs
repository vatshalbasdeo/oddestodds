﻿using Microsoft.AspNet.Identity;
using Oddestoodds.ServicesInterface.Interfaces;
using Oddestoodds.ServicesInterface.Models;
using Oddestoodds.Utilities;
using ODDESTOODDS.WebUI.Helpers;
using ODDESTOODDS.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ODDESTOODDS.WebUI.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IAdminServiceInterface _adminServiceInterface;

        public AdminController(IAdminServiceInterface adminServiceInterface)
        {
            _adminServiceInterface = adminServiceInterface;
        }
        
        // GET: Admin
        public ActionResult Index()
        {
            // currently active quotes
            var current = _adminServiceInterface.GetQuotes();

            //get quotes not yet published
            var notPublished = _adminServiceInterface.GetQuotestoPublish();

            //build the viewmodel for the admin dashboard
            var vm = new AdminDashbordViewModel { Current = current, NotPublished = notPublished };

            return View(vm);
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
 
            return View();
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var userId = User.Identity.GetUserId();
            try
            {
                // create the quote object inputted by the user
                var quote = new QuotesItem
                {
                    Away = collection["Away"],
                    Home = collection["Home"],
                    DrawQuote = Convert.ToDecimal(collection["DrawQuote"]),
                    AwayWinQuote = Convert.ToDecimal(collection["AwayWinQuote"]),
                    HomeWinQuote = Convert.ToDecimal(collection["HomeWinQuote"])
                };

                // Insert the quote and get status of the process
                var result = _adminServiceInterface.CreateQuote(quote, userId);
                if (result.Status)
                {
                    ViewBag.ResultMessage = result.Message;
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ResultMessage = result.Message;
                    return View(quote);
                }
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
                ViewBag.ResultMessage = "An Error occured";
            }
            return View();
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(Guid id)
        {
            // load quote selected for edit 
            var result = _adminServiceInterface.GetQuotesById(id);
            return View(result);
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, FormCollection collection)
        {

            var userId = User.Identity.GetUserId();
            try
            {
                // Build object from form and update
                var quote = new QuotesItem
                {
                    Id = id,
                    Away = collection["Away"],
                    Home = collection["Home"],
                    DrawQuote = Convert.ToDecimal(collection["DrawQuote"]),
                    AwayWinQuote = Convert.ToDecimal(collection["AwayWinQuote"]),
                    HomeWinQuote = Convert.ToDecimal(collection["HomeWinQuote"])
                };
                var result = _adminServiceInterface.UpdateQuote(quote, userId);
                if (result.Status)
                {
                    ViewBag.ResultMessage = result.Message;
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ResultMessage = result.Message;
                    return View(quote);
                }
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            return View();
        }

        // GET: Admin/Delete/5
        public JsonResult Delete(Guid id)
        {
            try
            {
                var userId = User.Identity.GetUserId();
                var quote = new QuotesItem { Id = id };

                // delete the quote
                var result = _adminServiceInterface.DeleteQuote(quote, userId);
                if (result.Status)
                {
                    // return success
                    return Json(new { Success = "True", Message = "Success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            //return failure
            return Json(new { error = "True", Message = "error" }, JsonRequestBehavior.AllowGet);
        }

        //GET: Admin/Rollback/5
        public ActionResult Rollback(Guid Id)
        {
            var userId = User.Identity.GetUserId();
            try
            {
                // delete the quote
                var result = _adminServiceInterface.RollBackStagedQuote(Id, userId);
                if (result.Status)
                {
                    // return success
                    return Json(new { Success = "True", Message = "Success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            //return failure
            return Json(new { error = "True", Message = "error" }, JsonRequestBehavior.AllowGet);
        }

        //GET: Admin/Publish
        public ActionResult Publish()
        {
            var userId = User.Identity.GetUserId();

            // call publish functionality using the userid logged in as identifier
            var result = _adminServiceInterface.PublishQuotes(userId);
            ViewBag.ResultMessage = result.Message;
            return RedirectToAction("Index");
        }
    }
}