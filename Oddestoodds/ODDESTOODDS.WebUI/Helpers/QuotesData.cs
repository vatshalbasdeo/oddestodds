﻿using ODDESTOODDS.WebUI.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ODDESTOODDS.WebUI.Helpers
{
    public class QuotesData
    {
        public object Get()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand sqlCommand = new SqlCommand(@"SELECT[Home]+' v/s '+[Away] AS Match,[HomeWinQuote],[DrawQuote],[AwayWinQuote] FROM [dbo].[Quotes]", connection))
                {
                    sqlCommand.Notification = null;
                    SqlDependency dependency = new SqlDependency(sqlCommand);
                    dependency.OnChange += new OnChangeEventHandler(Dependency_OnChange);
                    SqlDependency.Start(connectionString);

                    if (connection.State == System.Data.ConnectionState.Closed)
                        connection.Open();
                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    var result = reader.Cast<IDataRecord>().
                        Select(x => new
                        {
                            Match = Convert.ToString(x["Match"]),
                            Home = Convert.ToString(x["HomeWinQuote"]),
                            Draw = Convert.ToString(x["DrawQuote"]),
                            Away = Convert.ToString(x["AwayWinQuote"])
                        }).ToList();
                    return result;
                }
            }
        }

        private void Dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            QuoteHub.Show();
        }
    }
}