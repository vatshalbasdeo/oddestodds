﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oddestoodds.ServicesInterface.Enums
{
    public enum ActionsEnum
    {
        Create = 1,
        Edit = 2,
        Delete = 3
    }
}
