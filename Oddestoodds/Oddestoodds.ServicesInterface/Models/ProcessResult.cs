﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oddestoodds.ServicesInterface.Models
{
    public class ProcessResult
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public Guid? Id { get; set; }

    }
}
