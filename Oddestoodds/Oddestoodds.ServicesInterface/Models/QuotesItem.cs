﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oddestoodds.ServicesInterface.Models
{
    public class QuotesItem
    {
        public Guid Id { get; set; }

        [Required]
        public string Home { get; set; }

        [Required]
        public string Away { get; set; }

        [Required]
        //[RegularExpression(@"^[0-9]+(\.[0-9]{1,2})$", ErrorMessage = "Valid Decimal number with maximum 2 decimal places.")]
        public decimal HomeWinQuote { get; set; }

        [Required]
        //[RegularExpression(@"^[0-9]+(\.[0-9]{1,2})$", ErrorMessage = "Valid Decimal number with maximum 2 decimal places.")]
        public decimal DrawQuote { get; set; }

        [Required]
        //[RegularExpression(@"^[0-9]+(\.[0-9]{1,2})$", ErrorMessage = "Valid Decimal number with maximum 2 decimal places.")]
        public decimal AwayWinQuote { get; set; }
    }
}
