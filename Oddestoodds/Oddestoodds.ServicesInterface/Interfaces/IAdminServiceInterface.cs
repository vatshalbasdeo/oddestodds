﻿using Oddestoodds.ServicesInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oddestoodds.ServicesInterface.Interfaces
{
    public interface IAdminServiceInterface
    {

        ProcessResult CreateQuote(QuotesItem quote, string userId);

        ProcessResult UpdateQuote(QuotesItem quote, string userId);

        ProcessResult DeleteQuote(QuotesItem quote, string userId);

        List<QuotesItem> GetQuotes();

        List<QuotesItem> GetQuotestoPublish();

        ProcessResult RollBackStagedQuote(Guid quote, string userId);

        QuotesItem GetQuotesById(Guid id);

        ProcessResult PublishQuotes(string userId);

    }
}
