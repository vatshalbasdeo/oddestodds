﻿using Oddestoodds.Data;
using Oddestoodds.ServicesInterface.Enums;
using Oddestoodds.ServicesInterface.Interfaces;
using Oddestoodds.ServicesInterface.Models;
using Oddestoodds.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oddestoodds.Repositories
{
    public class AdminRepository : IAdminServiceInterface
    {
        private DBEntities _db;
        private const string SP_PUBLISHCHANGES = "SP_PUBLISHCHANGES @UserId";

        public AdminRepository(DBEntities db)
        {
            _db = db;
        }

        public ProcessResult CreateQuote(QuotesItem quote, string userId)
        {
            try
            {
                var newQuote = new QuotesToPublish
                {
                    Id = Guid.NewGuid(),
                    Home = quote.Home,
                    Away = quote.Away,
                    HomeWinQuote = quote.HomeWinQuote,
                    AwayWinQuote = quote.AwayWinQuote,
                    DrawQuote = quote.DrawQuote,
                    Action = (int)ActionsEnum.Create,
                    UserId = userId
                };
                _db.QuotesToPublishes.Add(newQuote);
                _db.SaveChanges();
                return new ProcessResult { Status = true, Message = "Success", Id = newQuote.Id };
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            return new ProcessResult { Status = false, Message = "Failure" };
        }

        public ProcessResult DeleteQuote(QuotesItem quote, string userId)
        {
            try
            {
                var alreadyModified = _db.QuotesToPublishes.Any(i => i.Id == quote.Id);
                if (alreadyModified)
                {
                    return new ProcessResult { Status = false, Message = "You are trying to delete a quote that has already being modified" };
                }
                else
                {
                    var dbQuote = _db.Quotes.Where(i => i.Id == quote.Id).SingleOrDefault();
                    if (dbQuote != null)
                    {
                        var deleteQuote = new QuotesToPublish
                        {
                            Id = dbQuote.Id,
                            Home = dbQuote.Home,
                            Away = dbQuote.Away,
                            HomeWinQuote = dbQuote.HomeWinQuote,
                            AwayWinQuote = dbQuote.AwayWinQuote,
                            DrawQuote = dbQuote.DrawQuote,
                            Action = (int)ActionsEnum.Delete,
                            UserId = userId
                        };

                        _db.QuotesToPublishes.Add(deleteQuote);
                        _db.SaveChanges();

                        return new ProcessResult { Status = true, Message = "Success", Id = deleteQuote.Id };
                    }

                }
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);

            }
            return new ProcessResult { Status = false, Message = "Failure" };
        }

        public List<QuotesItem> GetQuotes()
        {
            try
            {
                var data = _db.Quotes.Select(i => new QuotesItem
                {
                    Id = i.Id,
                    Home = i.Home,
                    HomeWinQuote = i.HomeWinQuote,
                    Away = i.Away,
                    AwayWinQuote = i.AwayWinQuote,
                    DrawQuote = i.DrawQuote,

                }).ToList();

                return data;
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            return null;
        }

        public QuotesItem GetQuotesById(Guid id)
        {
            try
            {
                var data = _db.Quotes.Where(i => i.Id == id).Select(i => new QuotesItem
                {
                    Id = i.Id,
                    Home = i.Home,
                    HomeWinQuote = i.HomeWinQuote,
                    Away = i.Away,
                    AwayWinQuote = i.AwayWinQuote,
                    DrawQuote = i.DrawQuote
                }).SingleOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            return null;
        }

        public List<QuotesItem> GetQuotestoPublish()
        {
            try
            {
                var data = _db.QuotesToPublishes.Select(i => new QuotesItem
                {
                    Id = i.Id,
                    Home = i.Home,
                    HomeWinQuote = i.HomeWinQuote,
                    Away = i.Away,
                    AwayWinQuote = i.AwayWinQuote,
                    DrawQuote = i.DrawQuote,

                }).ToList();

                return data;
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            return null;
        }

        public ProcessResult RollBackStagedQuote(Guid quoteId, string userId)
        {
            try
            {
                var dbQuote = _db.QuotesToPublishes.Where(i => i.Id == quoteId && i.UserId.Equals(userId)).SingleOrDefault();

                if (dbQuote != null)
                {
                    _db.QuotesToPublishes.Remove(dbQuote);
                    _db.SaveChanges();
                    return new ProcessResult { Status = true, Message = "Success", Id = dbQuote.Id };
                }

            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);

            }
            return new ProcessResult { Status = false, Message = "Failure" };
        }

        public ProcessResult UpdateQuote(QuotesItem quote, string userId)
        {
            try
            {
                var alreadyModified = _db.QuotesToPublishes.Any(i => i.Id == quote.Id);
                if (alreadyModified)
                {
                    return new ProcessResult { Status = false, Message = "You are trying to modify a quote that has already being modified" };
                }
                else
                {
                    var dbQuote = _db.Quotes.Where(i => i.Id == quote.Id).SingleOrDefault();
                    if (dbQuote!=null)
                    {
                        var editQuote = new QuotesToPublish
                        {
                            Id = dbQuote.Id,
                            Home = quote.Home,
                            Away = quote.Away,
                            HomeWinQuote = quote.HomeWinQuote,
                            AwayWinQuote = quote.AwayWinQuote,
                            DrawQuote = quote.DrawQuote,
                            Action = (int)ActionsEnum.Edit,
                            UserId = userId
                        };

                        _db.QuotesToPublishes.Add(editQuote);
                        _db.SaveChanges();
                        return new ProcessResult { Status = true, Message = "Success", Id = editQuote.Id };
                    }
                    
                }
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);

            }
            return new ProcessResult { Status = false, Message = "Failure" };
        }

        public ProcessResult PublishQuotes(string userId)
        {
            try
            {
                var result = _db.Database.ExecuteSqlCommand(SP_PUBLISHCHANGES, new SqlParameter("@UserId", userId));
                return new ProcessResult { Status = true, Message = "Success" };
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new ProcessResult { Status = false, Message = "Failure" };
        }
    }
}
