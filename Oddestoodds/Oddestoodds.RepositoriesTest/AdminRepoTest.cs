﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oddestoodds.Data;
using Oddestoodds.Repositories;
using Oddestoodds.ServicesInterface.Enums;
using Oddestoodds.ServicesInterface.Models;

namespace Oddestoodds.RepositoriesTest
{
    [TestClass]
    public class AdminRepoTest
    {
        [TestMethod]
        public void CreateQuotesTest()
        {
            // Arrange
            var userId = "AEBA5032-97B0-435B-9B1C-715EA6AB1E4E";
            var testObject = new QuotesItem
            {
                Home = "HOME",
                Away = "Away",
                HomeWinQuote = (decimal)1.1,
                AwayWinQuote = (decimal)1.1,
                DrawQuote = (decimal)1.1
            };


            // Act
            var context = new DBEntities();
            var repository = new AdminRepository(context);
            var result = repository.CreateQuote(testObject, userId);
            if (result.Status)
            {
                var expected = new QuotesToPublish
                {
                    Id = (Guid)result.Id,
                    Action = (int)ActionsEnum.Create,
                    Away = testObject.Away,
                    AwayWinQuote = testObject.AwayWinQuote,
                    DrawQuote = testObject.DrawQuote,
                    Home = testObject.Home,
                    HomeWinQuote = testObject.HomeWinQuote,
                    UserId = userId
                };
                //Assert
                var actual = context.QuotesToPublishes.Where(x => x.Id == result.Id).Single();
                Assert.AreEqual(expected.Action, actual.Action);
                Assert.AreEqual(expected.Away, actual.Away);
                Assert.AreEqual(expected.AwayWinQuote, actual.AwayWinQuote);
                Assert.AreEqual(expected.DrawQuote, actual.DrawQuote);
                Assert.AreEqual(expected.Home, actual.Home);
                Assert.AreEqual(expected.HomeWinQuote, actual.HomeWinQuote);
                Assert.AreEqual(expected.UserId, actual.UserId);

            }
        }

        [TestMethod]
        public void EditQuotesTest()
        {
            // Arrange
            var userId = "AEBA5032-97B0-435B-9B1C-715EA6AB1E4E";
            var testObject = new QuotesItem
            {
                Id = Guid.Parse("BF13D5D7-EF9B-441E-8FAB-B18A4C5099A9"),
                Home = "HOMEUpdated",
                Away = "AwayUpdated",
                HomeWinQuote = (decimal)1.2,
                AwayWinQuote = (decimal)1.2,
                DrawQuote = (decimal)1.2
            };


            // Act
            var context = new DBEntities();
            var repository = new AdminRepository(context);
            var result = repository.UpdateQuote(testObject, userId);
            if (result.Status)
            {
                var expected = new QuotesToPublish
                {
                    Id = (Guid)result.Id,
                    Action = (int)ActionsEnum.Edit,
                    Away = testObject.Away,
                    AwayWinQuote = testObject.AwayWinQuote,
                    DrawQuote = testObject.DrawQuote,
                    Home = testObject.Home,
                    HomeWinQuote = testObject.HomeWinQuote,
                    UserId = userId
                };
                //Assert
                var actual = context.QuotesToPublishes.Where(x => x.Id == result.Id).Single();
                Assert.AreEqual(expected.Action, actual.Action);
                Assert.AreEqual(expected.Away, actual.Away);
                Assert.AreEqual(expected.AwayWinQuote, actual.AwayWinQuote);
                Assert.AreEqual(expected.DrawQuote, actual.DrawQuote);
                Assert.AreEqual(expected.Home, actual.Home);
                Assert.AreEqual(expected.HomeWinQuote, actual.HomeWinQuote);
                Assert.AreEqual(expected.UserId, actual.UserId);

            }
        }

        [TestMethod]
        public void DeleteQuotesTest()
        {
            var context = new DBEntities();
            // Arrange
            var userId = "AEBA5032-97B0-435B-9B1C-715EA6AB1E4E";
            var quoteId = Guid.Parse("BF13D5D7-EF9B-441E-8FAB-B18A4C5099A9");
            var testObject = context.Quotes
                .Where(i => i.Id == quoteId)
                .Select(i => new QuotesItem
                {
                    Id = i.Id,
                    Home = i.Home,
                    Away = i.Away,
                    HomeWinQuote = i.HomeWinQuote,
                    AwayWinQuote = i.AwayWinQuote,
                    DrawQuote = i.DrawQuote
                }).SingleOrDefault();


            // Act

            var repository = new AdminRepository(context);
            var result = repository.DeleteQuote(testObject, userId);
            if (result.Status)
            {
                var expected = new QuotesToPublish
                {
                    Id = (Guid)result.Id,
                    Action = (int)ActionsEnum.Delete,
                    Away = testObject.Away,
                    AwayWinQuote = testObject.AwayWinQuote,
                    DrawQuote = testObject.DrawQuote,
                    Home = testObject.Home,
                    HomeWinQuote = testObject.HomeWinQuote,
                    UserId = userId
                };
                //Assert
                var actual = context.QuotesToPublishes.Where(x => x.Id == result.Id).Single();
                Assert.AreEqual(expected.Action, actual.Action);
                Assert.AreEqual(expected.Away, actual.Away);
                Assert.AreEqual(expected.AwayWinQuote, actual.AwayWinQuote);
                Assert.AreEqual(expected.DrawQuote, actual.DrawQuote);
                Assert.AreEqual(expected.Home, actual.Home);
                Assert.AreEqual(expected.HomeWinQuote, actual.HomeWinQuote);
                Assert.AreEqual(expected.UserId, actual.UserId);

            }
        }
    }
}
